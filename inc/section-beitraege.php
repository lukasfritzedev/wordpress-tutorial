<section id="beitraege">
    <h2>Beiträge verwalten <small class="subhead">bearbeiten, erstellen, organisieren</small>
    </h2>
    <p>
        Einen großen Teil von WordPress bilden die Beiträge. Sind sind die dynamischen Inhalte von der die Homepage lebt und – wenn man so will – der Kern des Ganzen. Mit ihnen ist
        die Blog-Funktion realisiert, aus der später das Content Management System in seiner jetzigen Form entstanden ist.

    </p>
    <h3 id="beitr-list">Die Beitragsliste <small class="subhead">Eine Übersicht aller Beiträge</small>
    </h3>
    <p>
        Über den Menüpunkt <em>Beiträge › Alle beiträge</em> <small>(direkt auf <em>Beiträge</em> klicken funktioniert auch)</small> gelangt man zu einer Liste aller Beiträge. Sind
        einige Beiträge vorhanden wird die Liste in mehrere Seiten aufgeteilt.
    </p>
    <p>
        Über der eigentliche Liste findet man einige Filteroptionen zum schnelleren Finden von Beiträgen, sowie eine Aktionsauswahl. Diese Massenbearbeitung ist hilfreich wenn die
        Informationen mehrerer Beiträge gleichzeitig bearbeitet werden sollen.
    </p>
    <p>
        Ja und dann gibt es natürlich noch die große Liste mit allen Beiträgen. Über einen Klick auf den <em>Beitragstitel</em> kann man einen Beitrag bearbeiten.
    </p>
    <p>
        Beiträge die einen anderen Status als <samp>veröffentlicht</samp> oder eine andere Sichtbarkeit als <samp>öffentlich</samp> haben, werden entsprechend markiert.
    </p>
    <p>
        In der Liste sind weitere Funktionen versteckt. Fährt man mit der Maus über einen Beiträgen erscheinen vier Optionen:
    </p>
    <ul>
        <li><em>Bearbeiten</em><br /> Zur Bearbeitungsansicht für den Beitrag. <small>Ein Klick auf den Beitragstitel klappt auch.</small>
        </li>
        <li><em>QuickEdit</em><br /> Öffnet die <a href="#beitr-quickedit" title="">QuickEdit-Funktionen</a>.
        </li>
        <li><em>Papierkorb</em><br /> Verschiebt den Beitrag in den Papierkorb. <small>Um einen Beitrag endgültig zu löschen muss der Papierkorb geleert oder der Beitrag aus dem
                Papierkorb entfernt werden</small> .
        </li>
        <li><em>Anschauen</em><br /> Zeigt den Beitrag in der Einzelbeitragsansicht im Frontend.
        </li>
    </ul>
    <p>
        Klickt man auf einen Autor, eine Kategorie oder ein Schlagwort wird die Beitragsliste entsprechend gefiltert. Ein kleine versteckte Funktion aber sehr nützlich bei vielen
        Beiträgen.
    </p>
    <h4 id="beitr-quickedit">Quickedit <small class="subhead">Schnell die Metadaten ändern</small>
    </h4>
    <p>
        Sollen nicht der Inhalt de Beitrags, sondern nur seine Metadaten geändert werden, gibt es die nützliche Funktion <em>QuickEdit</em>. Diese Funktion ermöglicht es einige
        Daten des Beitrags zu ändern ohne in die Bearbeitungsansicht wechseln zu müssen Alle was Sie im QuickEdit anpassen können, können Sie natürlich auch über den MetaBoxen der
        Bearbeitungsansicht erreichen, aber warum eine neue Ansicht laden, wenn es auch schneller geht.
    </p>
    <p>
        Befinden Sie sich in der Beitragsliste können Sie die <em>QuickEdit</em>-Änderungen aufrufen, in dem Sie mit der Maus über den zu ändernden Beitrag fahren und den Punkt
        <em>QuickEdit</em> aufrufen. Es erscheinen nun einige Eingabefelder zum Ändern der Metadaten.
    </p>
    <p>
        Auf der Linken Seite können sie den <em>Titel</em> des Beitrages und den <em>Permalink</em> ändern. Der Permalink ist der Beitragstitel wie er in der URL angezeigt wird.
        Außerdem können Sie hier das <em>Veröffentlichungsdatum</em> verändern und den Beitrag mit einem <em>Passwort</em> schützen oder ihn auf den Status <em>privat</em> setzen,
        sodass nur berechtigte Nutzer ihn lesen können.
    </p>
    <p>
        Weiter rechts steht die Auswahl der <em><a href="#kategorien" title="">Kategorien</a></em> zur Verfügung und es können <em><a href="#tags" title="">Schlagworte /
                Tags</a></em> hinzugefügt oder entfernt werden. Zusätzlich kann hier der <em>Veröffentlichungsstatus</em> geändert werden. Mit der Aktivierung der Option <em>Diesen
            Beitrag oben halten</em> wird der Beitrag nicht – wie für Blogbeiträge standardmäßig eingestellt – im Laufe der Zeit, also mit dem erstellen neuer Einträge, im Archiv
        nach unten abrutschen, sondern immer am Anfang der Beitragsliste im Frontend angezeigt.
    </p>
    <p>
        Haben Sie die gewünschten Änderungen vorgenommen bestätigen Sie diese mit einem Klick auf <em>Aktualisieren</em>. Anschließend müssen Sie die Änderungen mit ein Klick auf
        <em>Übernehmen</em> noch speichern. Das nicht vergessen. Mir ist es jetzt schon öfters passiert, dass ich einen Beitrag bearbeitet habe und dann nicht an das Übernehmen
        gedacht habe.
    </p>
    <h4 id="beitr-massenbearbeitung">Massenbearbeitung <small class="subhead">Bearbeiten mehrerer Beiträge</small>
    </h4>
    <p>
        Manchmal kommt es vor, dass die MetaDaten mehrerer Beiträge auf einmal geändert werden müssen. Dank der Funktion der <em>Massenbearbeitung</em> führt das allerdings nicht
        zu nerviger Fleißarbeit, sondern ist mit wenigen Klicks erledigt.
    </p>
    <p>
        Markieren Sie zunächst die zu ändernden Beiträge in der Beitragsliste indem Sie das Häkchen davor setzen. Haben Sie Ihre Auswahl getätigt wählen Sie über der Beitragsliste
        im Auswahlfeld <em>Aktion wählen</em> den Punkt bearbeiten und Klicken anschließend auf <em>Übernehmen</em>. Es öffnen sich nun die Eingabefelder für die Massenbearbeitung.
    </p>
    <p>
        Auf der linken Seite sehen Sie die Liste der gewählten Beiträge die bearbeitet werden. Durch einen Klick auf das kleine × neben einem Beitragstitel können Sie den Beitrag
        aus der Liste entfernen.
    </p>
    <p>
        Rechts finden sie – analog zum QuickEdit – die Felder für <em><a href="#kategorien" title="">Kategorien</a></em> und <em><a href="#tags" title="">Schlagworte /
                Tags</a></em>. Ist eine Kategorie nicht ALLEN Beiträgen der Auswahl zugewiesen, oder anders gesagt ist EIN Beitrag nicht in der Kategorie eingeordnet, ist das
        Häkchen in der Kategorieliste nicht gesetzt. <span style="color:red">Was passiert wenn der Haken gesetzt und dann wieder entfernt wird???</span> Für die Schlagworte gilt
        das analog.
    </p>
    <p>
        Darunter stehen einige Auswahlfelder zur Verfügung mit denen Sie den <em>Autor</em>, den <em>Status</em>, Die <em>Formatvorlage</em> (Beitragstyp) und weiteres ändern
        können.
    </p>
    <h3 id="beitr-erstellen">Einen Beitrag erstellen <small class="subhead">Neue Inhalte für Ihre Homepage</small>
    </h3>
    <p>
        Es gibt mehrere Stellen an denen man die Funktion zum Erstellen eines neuen Beitrags aufrufen kann. Alle führen im Endeffekt zum selben Ergebnis: man landet in der
        Bearbeitungsansicht für den neuen Beitrag.
    </p>
    <ul>
        <li><strong>Im Menü</strong><br /> Im Admin-Menü gibt es den Punkt <em>Beiträge › Erstellen</em>. <small>Befindet man sich bereits bei den Beiträgen ist der Menüpunkt
                sichtbar, ansonsten erscheint er, wenn man die Maus über den Punkt <em>Beiträge</em> bewegt. </small>
        </li>
        <li><strong>In der Admin-Bar</strong><br /> In der Admin-Bar findet man unter <em>+ Neu</em> einen Punkt <em>Beitrag</em>.
        </li>
        <li><strong>Neben der Überschrift</strong><br /> Befindet man sich in der Beitragsliste oder in der Bearbeitungsansicht eines Beitrags gibt es oben neben der Überschrift
            einen Button <em>Erstellen</em>.
        </li>
    </ul>
    <p>
        Nachdem Sie einen dieser Punkte gewählt haben können Sie den …
    </p>
    <h3 id="beitr-bearbeiten">Einen Beitrag bearbeiten <small class="subhead">Vorhandenes aktualisieren</small>
    </h3>
    <p>
        <em>Bitte Beachten Sie den Abschnitt <strong><a href="#bearbeitungsansicht" title="">Bearbeitungsansicht</a></strong> für allgemeine Informationen, insbesondere die
            Hinweise zum <a href="#editor" title="">Editor</a>. Hier werden nur die Besonderheiten für Beiträge behandelt.</em>
    </p>
    <p>
        <em>Bilder bzw. Medien sind ein eigenes, etwas größeres Kapitel und werden daher auch in dieser Anleitung in einem <a href="#medien" title="">eigenen Abschnitt</a>
            behandelt. Dort wird auch erklärt wie Bilder korrekt hochgeladen und in einen Beitrag eingefügt werden. <a href="#medien" title="">Zum Abschnitt Medien.</a></em>
    </p>
    <p>
        Rechts neben dem Hauptbereich der <a href="#bearbeitungsansicht" title="">Bearbeitungsansicht</a>, also dem Feld für den Titel und dem eigentlichen
        <a href="#editor" title="">Editor</a> <small>(werden im <a href="#bearbeitungsansicht" title="">Abschnitt Bearbeitungsansicht</a> ausführlich beschrieben)</small> ,
        befinden sich die MetaBoxen für die Seite. Hier können Sie zusätzliche zur Seite gehörige Daten, sog. MetaDaten angeben und anpassen. Die Box <em>Veröffentlichen</em> ist
        bei allen Beitragstypen gleich und wird daher auch schon im <a href="#bearbeitungsansicht" title="">Abschnitt Bearbeitungsansicht</a> beschrieben. Die anderen MetaBoxen
        sollen hier erklärt werden.
    </p>
    <p>
        Neben den vorgestellten MetaBoxen kann es auch noch weitere Boxen geben. Diese werden dann z.B. von Plugins hinzugefügt und können sehr unterschiedlich sein. Lesen Sie dazu
        bitte die Beschreibung des Plugins.
    </p>
    <p>
        …
    </p>
    <h3 id="kategorien">Kategorien <small class="subhead">Beiträge in Ordner stecken</small>
    </h3>
    <p>
        …
    </p>
    <h3 id="tags">Schlagworte / Tags <small class="subhead">Häufig gesuchte Begriffe</small>
    </h3>
    <p>
        …
    </p>
</section>