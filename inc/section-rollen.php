<section id="rollen">
    <h2>Nutzerrollen <small class="subhead">Rechte und Fähigkeiten</small>
    </h2>
    <p>
        WordPress nutzt ein Konzept von Nutzerrollen und -fähigkeiten um zu steuern was ein Nutzer auf der Seite darf und was nicht. Ich möchte hier einen kurzen Überblick über die
        Rollen geben, da es sein kann, dass einige der beschriebenen Funktionen für sie nicht sichtbar oder erreichbar sind. Dann hat Ihre Nutzerrolle vermutlich nicht die
        benötigten Rechte um die Funktion zu nutzen.
    </p>
    <p>
        Folgende Rollen sind in WordPress vorhanden:
    </p>
    <ul>
        <li><strong>Administrator</strong><br /> Der Administrator ist der Chef auf der Seite. Er hat Zugriff auf Alles.
        </li>
        <li><strong>Redakteur</strong><br /> Ein Redakteur kann Beiträgen und Seiten verwalten, insbesondere auch die von anderen Nutzern.
        </li>
        <li><strong>Autor</strong><br /> Ein Autor kann seine eigenen Beiträge verwalten.
        </li>
        <li><strong>Mitarbeiter</strong><br /> Ein Mitarbeiter kann seine eigenen Beiträge verwalten und neue Beiträge schreiben, aber nicht veröffentlichen.
        </li>
        <li><strong>Abonnent</strong><br /> Ein Abonnent kann sich im Backend einloggen und sein Profil bearbeiten. Diese Rolle ist sinnvoll, wenn Beiträge nicht öffentlich sein
            sollen sondern nur für eingeloggte Nutzer.
        </li>
    </ul>
    <p class="small">
        Zusätzlich zu den Standardrollen kann es auch weitere Rollen geben. So kann beispielsweise ein Plugin eine Rolle definieren deren Nutzer zwar Zugriff auf die Menüs haben,
        aber nicht auf andere Einstellungen des Themes.
    </p>
</section>