<!DOCTYPE html>
<head lang="de">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />

    <title>WordPress für Einsteiger</title>

    <meta name="description" content="Ein kleine Beschreibung für die Verwaltung der Inhalte einer WordPress-Seite" />
    <meta name="author" content="Lukas Fritze" />

    <!-- --------------------------------------------------------------------------------*/
    /* Support for Apple Webpage-Icon
    /* Sizes:  60x60, 76x76, 120x120, 152x152
    /*--------------------------------------------------------------------------------- -->
    <link rel="apple-touch-icon" href="img/touch-icon/touch-icon-iPhone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon/touch-icon-iPad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/touch-icon/touch-icon-iPhone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon/touch-icon-iPad-retina.png">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="fancybox/jquery.fancybox.pack.js"></script>

    <script src="js/min/index.min.js"></script>

    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="60">
<div class="container">
    <div class="row">
        <?php require 'inc/nav.php' ?>

        <div class="col-sm-8">
            <?php require 'inc/header.php' ?>

            <!-- Einleitung  --------  -->
            <?php require 'inc/section-einleitung.php' ?>
            <!-- Einleitung  --------  -->
            <?php require 'inc/section-das-wichtigste.php' ?>
            <!-- Begriffe  --------  -->
            <?php require 'inc/section-begriffe.php' ?>
            <!-- Roles  --------  -->
            <?php require 'inc/section-rollen.php' ?>
            <!-- Backend  --------  -->
            <?php require 'inc/section-backend.php' ?>
            <!-- Beiträge  --------  -->
            <?php require 'inc/section-beitraege.php' ?>
            <!-- Seiten  --------  -->
            <?php require 'inc/section-seiten.php' ?>
            <!-- Medien  --------  -->
            <?php require 'inc/section-medien.php' ?>
            <!-- Menü  --------  -->
            <?php require 'inc/section-menues.php' ?>
            <!-- Widgets  --------  -->
            <?php require 'inc/section-widgets.php' ?>
            <!-- Shortcodes  --------  -->
            <?php require 'inc/section-shortcodes.php' ?>
        </div>
    </div>

    <footer class="row">
        <div class="col-sm-4 small contact">
            <h4>Lukas Fritze</h4>
            <p>
                <a href="http://fritzedesign.de" title="Lukas Fritze Design">www.fritzedesign.de</a><br />
                <a href="mailto:web@fritzedesign.de" title="Mail an Lukas Fritze">web@fritzedesign.de</a><br />
                &copy; <?php date_default_timezone_set( 'Europe/Berlin' );
                echo date( 'Y' ); ?> <a href="http://fritzedesign.de/impressum/" title="Impressum">Impressum</a><br />
            </p>
        </div>
    </footer>
</div>
</body>


	